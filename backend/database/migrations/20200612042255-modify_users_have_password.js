
module.exports = {
  up: (queryInterface, Sequelize) => Promise.all([
    queryInterface.addColumn(
      'Users', // table name
      'password', // new field name
      {
        type: Sequelize.STRING,
        allowNull: true,
      },
    ),
  ]),


  down: (queryInterface, Sequelize) => Promise.all([
    queryInterface.removeColumn('Users', 'password'),
  ]),
};
