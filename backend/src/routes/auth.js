const express = require('express');
const { signUp, signIn } = require('../controllers/userController');
const {healthCheck} = require('../controllers/healthController');
const {
  userSignupValidator, userSignInValidator, runValidation, isJwtValid,
} = require('../validators/auth');

const router = express.Router();

router.post('/signup', userSignupValidator, runValidation, signUp);
router.post('/signin', userSignInValidator, runValidation, signIn);
router.get('/health', healthCheck);
router.get('/secret', isJwtValid, (req, res) => {
  res.json({ message: 'secret' });
});

module.exports = router;
