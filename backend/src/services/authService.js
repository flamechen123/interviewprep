const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
require('dotenv').config();

module.exports = {
  async hashPassword(password) {
    const saltRounds = 10;
    const passwordHash = await bcrypt.hash(password, saltRounds);
    return passwordHash;
  },

  async verifyPassword(user, inputPassword) {
    const passwordIsCorrect = user === null ? false : await bcrypt.compare(inputPassword, user.password);
    return passwordIsCorrect;
  },

  async generateToken(user) {
    const userToken = {
      username: user.name,
      id: user.id,
    };

    const token = await jwt.sign(userToken, process.env.JWT_SECRET, { expiresIn: '1d' });
    return token;
  },
};
