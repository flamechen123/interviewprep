const models = require('../../database/models');
const authService = require('../services/authService');

const signUp = async (req, res, next) => {
  try {
    const { email, name, password } = req.body;

    const hashedPassword = await authService.hashPassword(password);

    await models.User.create({
      name,
      email,
      password: hashedPassword,
    });

    return res.status(201).json({ message: 'Sign up was successfull' });
  } catch (err) {
    console.log('Error on signup', err);
    return res.status(400).json({
      error: err,
    });
  }
};


const signIn = async (req, res, next) => {
  try {
    const user = await models.User.findAll({
      where: {
        name: req.body.name,
      },
    });

    if(user.length === 0){
        return res.status(404).send({message: "User not found"})
    }
    
    const passwordIsCorrect = await authService.verifyPassword(user[0].dataValues, req.body.password);

    if (!(user && passwordIsCorrect)) {
      return res.status(401).json({
        error: 'Invalid username or password',
      });
    }

    const token = await authService.generateToken(user);

    return res.status(200).send({ token, name: user[0].dataValues.name });
  } catch (err) {
    console.log('Error upon signing in', err);
    res.status(400).json({
      error: err,
    });
    next();
  }
};

module.exports = {
  signUp,
  signIn,
};
