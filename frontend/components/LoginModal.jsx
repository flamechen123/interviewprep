import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import InputAdornment from '@material-ui/core/InputAdornment';
import EmailIcon from '@material-ui/icons/Email';
import LockIcon from '@material-ui/icons/Lock';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import IconButton from '@material-ui/core/IconButton';
import { makeStyles } from '@material-ui/core/styles';
import CloseIcon from '@material-ui/icons/Close';
import Link from '@material-ui/core/Link';
import Box from '@material-ui/core/Box';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import clsx from 'clsx';


const useStyles = makeStyles((theme) => ({
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
  signUpButton: {
    padding: theme.spacing(3)
  },
  forgotPasswordButton: {
    "margin-bottom": theme.spacing(3)
  },
  signUpButton: {
    "margin-bottom": theme.spacing(3)
  },
  button: {
    borderColor: "black",
    color: "black"
  }
}));

function LoginModal({ setOpen, open, className=0 ,setSignUpOpen, ...rest }) {
  const classes = useStyles();
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.down('sm'));

  const [values, setValues] = React.useState({
    password: '',
    showPassword: false,
  });

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleChange = (prop) => (event) => {
    setValues({ ...values, [prop]: event.target.value });
  };

  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword });
  };

  return (
    <>
      <Button variant="outlined" className={clsx(classes.button, className)} onClick={handleClickOpen} {...rest}>
        {" Log In "}
      </Button>
      <Dialog maxWidth="xs" open={open} onClose={handleClose} aria-labelledby="Sign Up-dialog">
        <DialogTitle id="form-dialog-title">
          Log In to Your Audiolize Accout!
          <IconButton aria-label="close" className={classes.closeButton} onClick={handleClose}>
            <CloseIcon />
          </IconButton>
        </DialogTitle>
        <DialogContent dividers>
          <TextField
            autoFocus
            margin="normal"
            id="emailText"
            label="Email Address"
            type="email"
            variant="outlined"
            fullWidth
            placeholder="Email"
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <EmailIcon />
                </InputAdornment>
              ),
            }}
          />
          <TextField
            id="password"
            type={values.showPassword ? 'text' : 'password'}
            value={values.password}
            onChange={handleChange('password')}
            label="Password"
            variant="outlined"
            margin="normal"
            fullWidth
            placeholder="Password"
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <LockIcon />
                </InputAdornment>
              ),
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={handleClickShowPassword}
                  >
                    {values.showPassword ? <Visibility /> : <VisibilityOff />}
                  </IconButton>
                </InputAdornment>
              )
            }}
          />
        </DialogContent>
        <DialogActions className={classes.signUpButton}>
          <Button fullWidth variant="contained" onClick={handleClose} fullWidth color="primary">
            Log In
          </Button>
        </DialogActions>
        <Link
            className={classes.forgotPasswordButton}
            component="button"
            variant="body2"
            onClick={() => {
              console.log("heere");
            }}
          >
            Forgot Password
        </Link>
        <Box alignSelf="center">
          <span style={{verticalAlign: "top"}}>
            {"Don't have an account? "}
          </span>
          <Link
              className={classes.signUpButton}
              component="button"
              variant="body2"
              onClick={()=>{setOpen(false); setSignUpOpen(true)}}
            >
             Sign up
          </Link>
        </Box>
      </Dialog>
    </>
  )
}

export default LoginModal;