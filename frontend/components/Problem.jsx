import {
  Tabs,
  Grid,
  makeStyles,
  Box,
  Tab,
  Button
} from "@material-ui/core";
import ProblemDescription from "./ProblemDescription";
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import NavigateBeforeIcon from '@material-ui/icons/NavigateBefore';
import ListIcon from '@material-ui/icons/List';

const styles = (theme) => ({
  tabs: {
    backgroundColor: '#f7f7f7'
  },
  activeTab: {
    backgroundColor: 'white'
  },
  tabPanel: {
    backgroundColor: "white",
    height: "100%"
  },
  tabIndictator: {
    backgroundColor: "white"
  },
  consoleButton: {
    marginRight: "auto"
  },
  problemAreaButtons: {
    paddingTop: theme.spacing(2.5),
    paddingBottom: theme.spacing(2.5),
    paddingLeft: theme.spacing(1.25),
    paddingRight: theme.spacing(1.25)
  },
  problemSetButton: {
    marginRight: "auto"
  },
  problemContainer: {
    height: "100%"
  }
});

const useStyles = makeStyles(styles);

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          {children}
        </Box>
      )}
    </div>
  );
}

function a11yProps(index) {
  return {
    id: `full-width-tab-${index}`,
    'aria-controls': `full-width-tabpanel-${index}`,
  };
}

function Problem({ problem }) {
  const classes = useStyles();
  const [tabValue, setTabValue] = React.useState(0);
  const handleTabChange = (event, newValue) => {
    setTabValue(newValue);
  };

  return (
    <Box width="100%" height="100%">
      <Grid container direction="column" className={classes.problemContainer}>
        <Grid item>
          <Tabs
            value={tabValue}
            onChange={handleTabChange}
            indicatorColor="secondary"
            textColor="inherit"
            variant="fullWidth"
            aria-label="custom tests"
            className={classes.tabs}
            TabIndicatorProps={{className: classes.tabIndictator}}
          >
            <Tab label="Description" className={tabValue == 0 ? classes.activeTab : ""} {...a11yProps(0)} />
            <Tab label="Solution" className={tabValue == 1 ? classes.activeTab : ""} {...a11yProps(1)} />
            <Tab label="Video" className={tabValue == 2 ? classes.activeTab : ""} {...a11yProps(2)} />
          </Tabs>
        </Grid>
        <Grid item style={{flexGrow: "1"}}>
          <TabPanel className={classes.tabPanel} value={tabValue} index={0}>
            <ProblemDescription problem={problem}/>
          </TabPanel>
          <TabPanel className={classes.tabPanel} value={tabValue} index={1}>
            Item Two
          </TabPanel>
          <TabPanel className={classes.tabPanel} value={tabValue} index={2}>
            Item Three
          </TabPanel>
        </Grid>
        <Grid item>
          <Grid container justify="flex-end" className={classes.problemAreaButtons} alignItems="center">
            <Grid item className={classes.problemSetButton}>
              <Button startIcon={<ListIcon />} variant="outlined">
                Current Problem Set
              </Button>
            </Grid>
            <Grid item>
              <Box marginRight={2}>
                <Button startIcon={<NavigateBeforeIcon />} variant="outlined">
                  Previous
                </Button>
              </Box>
            </Grid>
            <Grid item>
              <Button startIcon={<NavigateNextIcon />} variant="outlined">
                Next
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Box>
  )
};

export default Problem;