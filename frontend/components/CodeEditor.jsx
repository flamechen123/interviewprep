import dynamic from 'next/dynamic';
const CodeMirror = dynamic(() => {
  import('codemirror/mode/javascript/javascript');
  import('codemirror/mode/python/python');
  import('codemirror/mode/clike/clike');
  return import("react-codemirror2").then((mod) => mod.Controlled);
}, { ssr: false });
import {
  Grid,
  Box
} from "@material-ui/core"
// let modeLoaded = false;
// if (typeof window !== 'undefined' && typeof window.navigator !== 'undefined') {
//   require('codemirror/mode/javascript/javascript');
//   require('codemirror/mode/python/python');
//   require('codemirror/mode/clike/clike');
//   modeLoaded = true;
// };

function CodeEditor({
  configOptions, 
  mode="javascript", 
  height="100%", 
  width="100%",
  setCodeInputs,
  codeInputs,
  fontSize="16px"
}){
  const codeMirrorOptions = {
    theme: "default",
    lineNumbers: true,
    scrollbarStyle: null,
    lineWrapping: true,
    ...configOptions
  };
  return CodeMirror && ( 
    <Box height={height} width={width} fontSize={fontSize}>
      <CodeMirror
        options={{
          ...codeMirrorOptions,
          mode: mode
        }}
        value={codeInputs ? codeInputs[mode] : ""}
        onBeforeChange={(editor, data, value) => {
          setCodeInputs({
            ...codeInputs,
            [mode]: value
          }); 
        }}
      /> 
    </Box>
  )
}

export default CodeEditor;