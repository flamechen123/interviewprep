import {
  Grid,
  Box,
  Typography,
  Divider,
  Link,
  Container
} from "@material-ui/core";

function Footer() {
  return (
    <Box mt={5} mb={2}>
      <Container>
          <Box mb={2}>
            <Divider />
          </Box>
          <Grid container justify="center">
            <Grid item>
              <Link
                color="textPrimary"
                href="https://material-ui.com/store/items/devias-kit-pro"
              >
                Contact Us
              </Link>
            </Grid>
            <Divider orientation="vertical" flexItem variant="middle"/>
            <Grid item>
              <Link
                color="textPrimary"
                href="https://material-ui.com/store/items/devias-kit-pro"
              >
                FAQ
              </Link>
            </Grid>
            <Divider orientation="vertical" flexItem variant="middle"/>
            <Grid item>
              <Link
                color="textPrimary"
                href="https://material-ui.com/store/items/devias-kit-pro"
              >
                Testimonial
              </Link>
            </Grid>
            <Divider orientation="vertical" flexItem variant="middle"/>
            <Grid item>
              <Link
                color="textPrimary"
                href="https://material-ui.com/store/items/devias-kit-pro"
              >
                Become an Affiliate
              </Link>
            </Grid>
            <Divider orientation="vertical" flexItem variant="middle"/>
            <Grid item>
              <Link
                color="textPrimary"
                href="https://material-ui.com/store/items/devias-kit-pro"
              >
                Privacy Policy
              </Link>
            </Grid>
          </Grid>
          <Box mt={1}>
            <Typography variant="body2" align="center">
              Copyright © 2000-2020 CleanCode, LLC. All rights reserved.
            </Typography>
          </Box>
      </Container>
    </Box>
  )
}

export default Footer;