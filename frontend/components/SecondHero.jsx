import {
  Box,
  Container,
  Grid,
  Typography,
  makeStyles,
} from '@material-ui/core';

const styles = (theme) => ({
  image: {
    width: "100%",
    height: "100%"
  },
  heroContainer: {
    marginTop: "100px"
  }
});

const useStyles = makeStyles(styles);

function SecondHero({ className, ...rest }) {
  const classes = useStyles();
  return (
    <Box mt={5}>
      <Container maxWidth="lg">
        <Grid container spacing={8}>
          <Grid item md={6}>
            <div>
              <img
                className={classes.image}
                alt="Presentation"
                src="/img/dark-light.png"
              />
            </div>
          </Grid>
          <Grid item md={6}>
            <Grid container direction="column" spacing={1}>
              <Grid item>
                <Typography variant="overline">
                  Second Section
                </Typography>
              </Grid>
              <Grid item>
                <Typography variant="h1">
                  Ace Behavior Interviews
                </Typography>
              </Grid>
              <Grid item>
                <Typography variant="body1">
                  Most Candidates are eliminated after the first round
                  of interview! First round interview usually consists 
                  of behavior questions that aim to see if the candidate
                  can meet the technical background that the position requires.
                  Learn to avoid pitfalls to seemingly innocuous questions,
                  and learn how to handle all behavior questions by crafting 
                  and memorizing only a few scenarios using our modified STAR techniques.
                  And learn from real world questions and solutions from real candidates.
                </Typography>
              </Grid>
              <Grid item>
                <Grid container spacing={3}>
                  <Grid item>
                    <Box maxWidth="10em">
                      <Typography variant="h1">
                        50+
                      </Typography>
                      <Typography variant="overline">
                        Example Scenarios 
                      </Typography>
                    </Box>
                  </Grid>
                  <Grid item>
                    <Box maxWidth="10em">
                      <Typography variant="h1">
                        10+
                      </Typography>
                      <Typography variant="overline">
                        Video Tutorials
                      </Typography>
                    </Box>
                  </Grid>
                  <Grid item>
                    <Box maxWidth="10em">
                      <Typography variant="h1">
                        100+
                      </Typography>
                      <Typography variant="overline">
                        Problems
                      </Typography>
                    </Box>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Container>
    </Box>
  )
}

export default SecondHero;