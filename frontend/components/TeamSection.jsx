import {
  Container,
  Grid,
  Box,
  Typography
} from "@material-ui/core";
import TeamCard from "./TeamCard";

function TeamSection() {
  const description1 = `Software Engineer for three years at Amazon, 
              then software engineers at uber,
              now senior software engineer at Google and 
              CleanCode. I realized far too late in my career that interviewing
              skill is complete seperate skill set than developing and I wish I can
              share my experience and learnings with others`;
  const description2 = `Software Software Engineer at Capital One for 3 years, then 
                        Senior Softare Engineeer at Google for 6 years, 
                        highly involved in google's interviewe process, and been on the
                        hiring committe for many years. I want to share 
                        what I learned with anyone interested`
  return (
    <Container maxWidth="lg">
      <Box margin={7}>
        <Typography variant="h1" align="center">
          Meet the Team
        </Typography>
      </Box>
      <Grid container justify="space-around">
        <Grid item>
          <TeamCard 
            avatarSrc="/img/profile1.jpeg"
            name="Mohamed Isse"
            title="Senior Software Engineer"
            logoSrc="img/googleLogo.svg"
            description={description1}
          />
        </Grid>
        <Grid item>
          <TeamCard 
            avatarSrc="/img/profile_liping.png"
            name="Liping Chen"
            title="Senior Software Engineer"
            logoSrc="img/googleLogo.svg"
            description={description2}
          />
        </Grid>
      </Grid>
    </Container>
  );
};

export default TeamSection;