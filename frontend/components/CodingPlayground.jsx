import {
  Grid,
  Box,
  InputLabel,
  MenuItem,
  FormHelperText,
  FormControl,
  Select,
  makeStyles,
  IconButton,
  Tabs,
  Tab,
  Typography,
  TextField,
  Button
} from "@material-ui/core";
import SettingsIcon from '@material-ui/icons/Settings';
import FullscreenIcon from '@material-ui/icons/Fullscreen';
import RefreshIcon from '@material-ui/icons/Refresh';
import CodeIcon from '@material-ui/icons/Code';
import CodeEditor from './CodeEditor';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const styles = (theme) => ({
  formControl: {
    minWidth: 120,
    maxHeight: "50px"
  },
  inputLabel: {
    color: "black"
  },
  langaugeDropDown: {
    marginRight: "auto"
  },
  testsInput: {
    backgroundColor: 'white'
  },
  tabs: {
    backgroundColor: '#f7f7f7'
  },
  activeTab: {
    backgroundColor: 'white'
  },
  tabPanel: {
    backgroundColor: "white"
  },
  tabIndictator: {
    backgroundColor: "white"
  },
  consoleButton: {
    marginRight: "auto"
  },
  codeAreaButtons: {
    paddingTop: theme.spacing(2.5),
    paddingBottom: theme.spacing(2.5),
    paddingLeft: theme.spacing(1.25),
    paddingRight: theme.spacing(1.25)
  },
  editorContainer: {
    height: "100%"
  }
});

const useStyles = makeStyles(styles);

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          {children}
        </Box>
      )}
    </div>
  );
}

function a11yProps(index) {
  return {
    id: `full-width-tab-${index}`,
    'aria-controls': `full-width-tabpanel-${index}`,
  };
}

function CodingPlaygroud({ problem }) {
  const [language, setLanguage] = React.useState("javascript");
  const [codeInputs, setCodeInputs] = React.useState({
    "javascript": problem.initialValues["javascript"],
    "python": problem.initialValues["python"],
    "text/x-java": problem.initialValues["text/x-java"]
  });
  const [tabValue, setTabValue] = React.useState(0);
  const [console, setConsole] = React.useState(true);
  const classes = useStyles();

  const handleChange = (event) => {
    setLanguage(event.target.value);
  };

  const handleClick = (event) => {
    if(event.target.id == "refresh-codeEditor"){
      localStorage.removeItem(`${problem.id}_${language}`);
      setCodeInputs({...codeInputs, [language]: problem.initialValues[language]})
    }
  };

  const handleTabChange = (event, newValue) => {
    setTabValue(newValue);
  };

  const handleConsoleToggle = (event, newValue) => {
    setConsole(!console);
  }

  React.useEffect(() => {
    const data = localStorage.getItem(`${problem.id}_lastLan`);
    if (data) setLanguage(data);
  }, []);

  React.useEffect(() => {
    const data = localStorage.getItem(`${problem.id}_${language}`);
    localStorage.setItem(`${problem.id}_lastLan`, language);
    if (data) setCodeInputs({...codeInputs, [language]: data});
  }, [language]);

  React.useEffect(() => {
    localStorage.setItem(`${problem.id}_${language}`, codeInputs[language]);
  }, [codeInputs]);

  return (
    <Box width="100%" height="100%">
      <Grid container direction="column" className={classes.editorContainer}>
        <Grid item>
          <Box width="100%" mt="20px">
            <Grid container justify="flex-end">
              <Grid item className={classes.langaugeDropDown}>
                <FormControl variant="outlined" color="secondary" className={classes.formControl}>
                  <InputLabel id="code-language-selection-label" className={classes.inputLabel}>Language</InputLabel>
                  <Select
                    labelId="language-selection-label"
                    id="language-select-outlined"
                    value={language}
                    onChange={handleChange}
                    label="Javascript"
                  >
                    <MenuItem value={"javascript"}>Javascript</MenuItem>
                    <MenuItem value={"python"}>Python</MenuItem>
                    <MenuItem value={"text/x-java"}>Java</MenuItem>
                  </Select>
                </FormControl>
              </Grid>
              <Grid item>
                <IconButton onClick={handleClick}>
                  <RefreshIcon id="refresh-codeEditor"/>
                </IconButton>
              </Grid>
              <Grid item>
                <IconButton>
                  <CodeIcon />
                </IconButton>
              </Grid>
              <Grid item>
                <IconButton>
                  <SettingsIcon />
                </IconButton>
              </Grid>
              <Grid item>
                <IconButton>
                  <FullscreenIcon />
                </IconButton>
              </Grid>
            </Grid>
          </Box>
        </Grid>
        <Grid item style={{flexGrow: 1}}>
          <Box mt="20px" width="100%" height="100%" position="relative">
            <CodeEditor 
              mode={language} 
              codeInputs={codeInputs} 
              setCodeInputs={setCodeInputs} 
              height="850px" 
            />
            {console && <Box position="absolute" bottom="0" left="0" right="0" zIndex={3}>
              <Tabs
                value={tabValue}
                onChange={handleTabChange}
                indicatorColor="secondary"
                textColor="inherit"
                variant="fullWidth"
                aria-label="custom tests"
                className={classes.tabs}
                TabIndicatorProps={{className: classes.tabIndictator}}
              >
                <Tab label="Local Test Cases" className={tabValue == 0 ? classes.activeTab : ""} {...a11yProps(0)} />
                <Tab label="Local Tests Result" className={tabValue == 1 ? classes.activeTab : ""} {...a11yProps(1)} />
                <Tab label="Server Tests Results" className={tabValue == 2 ? classes.activeTab : ""} {...a11yProps(2)} />
              </Tabs>
              <TabPanel className={classes.tabPanel} value={tabValue} index={0}>
                <TextField
                  id="outlined-multiline-static"
                  multiline
                  rows={4}
                  defaultValue="Default Value"
                  variant="outlined"
                  fullWidth
                  InputProps={{
                    className: classes.testsInput
                  }}
                />
              </TabPanel>
              <TabPanel className={classes.tabPanel} value={tabValue} index={1}>
                Item Two
              </TabPanel>
              <TabPanel className={classes.tabPanel} value={tabValue} index={2}>
                Item Three
              </TabPanel>
            </Box>}
          </Box>
        </Grid>
        <Grid item>
          <Grid container justify="flex-end" className={classes.codeAreaButtons} alignItems="center">
            <Grid item className={classes.consoleButton}>
              <Button 
                endIcon={console ? <ExpandMoreIcon />: <ExpandLessIcon />} 
                onClick={handleConsoleToggle}>
                Console
              </Button>
            </Grid>
            <Box marginRight={2}>
              <Grid item>
                <Button variant="outlined">
                  Run Code
                </Button>
              </Grid>
            </Box>
            <Grid item>
              <Button variant="contained">
                Submit 
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Box>
  );
}

export default CodingPlaygroud;