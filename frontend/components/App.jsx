import { ThemeProvider } from '@material-ui/core/styles';
import theme from "./ui/Theme";
import Header from "./Header";
import Landing from "./Landing";

function App() {

  return (
    <ThemeProvider theme={theme}>
      <Landing />
    </ThemeProvider>
  );
}

export default App