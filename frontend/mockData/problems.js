const jsInitialCode =`/**
 * @param {string[]} logs
 * @return {string[]}
 */
var reorderLogFiles = function(logs) {
    
};`;

const pythonInitialCode = `class Solution(object):
    def reorderLogFiles(self, logs):
        """
        :type logs: List[str]
        :rtype: List[str]
        """`;

const javaInitialCode = `class Solution {
    public String[] reorderLogFiles(String[] logs) {
        
    }
}`;

const reorderDataInLogFiles = {
  id: "123",
  initialValues: {
    "javascript": jsInitialCode,
    "python": pythonInitialCode,
    "text/x-java": javaInitialCode
  },
  title: "Two Sum",
  difficulty: "Easy",
  freqeuncy: "600",
  paragraphs: ["Given an array of integers, return indices of the two numbers such that they add up to a specific target.", 
  "You may assume that each input would have exactly one solution, and you may not use the same element twice."],
  examples: [`Given nums = [2, 7, 11, 15], target = 9,

Because nums[0] + nums[1] = 2 + 7 = 9,
return [0, 1].`],
  acceptanceRate: "63%"
}

module.exports = {
  reorderDataInLogFiles
}