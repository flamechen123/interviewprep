import CodingPlayground from "../components/CodingPlayground";
import Problem from "../components/Problem"
import * as problems from "../mockData/problems";
import {
  Grid,
  Box,
  makeStyles
} from "@material-ui/core"
import dynamic from 'next/dynamic';
// const SplitterLayout = dynamic(() => {
//   return import("react-splitter-layout");
// }, { ssr: false });
import {
  ReflexContainer,
  ReflexSplitter,
  ReflexElement
} from 'react-reflex';

const styles = (theme) => ({
  splitter: {
    width: "5px"
  }
});

const useStyle = makeStyles(styles);

function CodeEditorPage() {
  const classes = useStyle();
  return (
    // <Grid container>
    //   <Grid item xs={6}>
    //     <Problem problem={problems.reorderDataInLogFiles}/>
    //   </Grid>
    //   <Grid item xs={6}>
    //     <CodingPlayground problem={problems.reorderDataInLogFiles}/>
    //   </Grid>
    // </Grid>
    // <SplitterLayout>
    //   <Problem problem={problems.reorderDataInLogFiles}/>
    //   <CodingPlayground problem={problems.reorderDataInLogFiles}/>
    // </SplitterLayout>
    <Box height="100vh">
      <ReflexContainer orientation="vertical">
        <ReflexElement className="left-pane">
          <Problem problem={problems.reorderDataInLogFiles}/>
        </ReflexElement>
        <ReflexSplitter className={classes.splitter}/>
        <ReflexElement className="right-pane">
          <CodingPlayground problem={problems.reorderDataInLogFiles}/>
        </ReflexElement>
      </ReflexContainer>
    </Box>
  );
};

export default CodeEditorPage;